# cli-todo-list

a command-line todo list

## Demo

[![asciicast](https://asciinema.org/a/ugdOK4yP8MpttXCgYFAVDbadr.svg)](https://asciinema.org/a/ugdOK4yP8MpttXCgYFAVDbadr)

## Requirements

- _Redis_
- _NVM_

## Install

To install run the following commands:

```
#
nvm i

npm i
```

## Run

```
npm run cli
```

## Test

```
npm t
```

## Redis

One way of running it is to use docker

```
docker run --name redis -p 6379:6379 -it redis
```
