import { createClient } from 'redis'
import { Config } from '../config'

export interface Redis {
  store(data: string): void
  fetch(): Promise<string>
}

export default (config: Config): Redis => {
  const redisKey = 'cli'
  const redisClient = createClient(config.redis)

  function store(data: string): void {
    redisClient.set(redisKey, data)
  }

  function fetch(): Promise<string> {
    return new Promise((resolve: Func, reject: Func) => {
      redisClient.get(redisKey, (err, data) => {
        if (err) {
          return reject(err)
        }

        resolve(data)
      })
    })
  }

  return {
    store,
    fetch
  }
}
