export interface Config {
  redis: {
    host: string
    port: number
  }
}

export const config: Config = {
  redis: {
    host: process.env.REDIS_HOST || '127.0.0.1',
    port: ~~process.env.REDIS_PORT || 6379
  }
}
