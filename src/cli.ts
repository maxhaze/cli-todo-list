#!/usr/bin/env node

import * as Prompts from 'prompts'

import CliOperations from './cli-operations'
import Redis from './libs/redis'
import StorageService from './services/storage'
import TasksService from './services/tasks'

import { config } from './config'
import { Commands } from './enums'

async function start(): Promise<boolean> {
  // dependencies
  const redis = Redis(config)
  const storageService = StorageService(redis)
  const tasksService = TasksService(storageService)
  const cliOperations = CliOperations(tasksService)

  const { mode } = await Prompts({
    type: 'select',
    name: 'mode',
    message: 'What would you like to do now',
    choices: [
      { title: 'Add new task', value: Commands.add },
      { title: 'List tasks / Mark as Complete', value: Commands.list },
      { title: 'Quit ...', value: Commands.quit }
    ]
  })

  switch (mode) {
    case Commands.add:
      await cliOperations.handleAdd()
      break
    case Commands.list:
      const tasks = await tasksService.getList()
      await cliOperations.handleList(tasks)
      break
    case Commands.quit:
      return false
  }

  return true
}

async function cli(): Promise<void> {
  let isProcessRunning = true
  while (isProcessRunning) {
    isProcessRunning = await start()
  }

  // Check if user wants to really quite
  const { value } = await Prompts({
    type: 'text',
    name: 'value',
    message: 'Would you like to quit?(yes|no)'
  })

  if (value === 'yes') {
    process.exit()
  }

  await cli()
}

;(async (): Promise<void> => {
  process.on('SIGINT', () => process.exit())

  try {
    await cli()
  } catch (error) {
    // Todo: Show some error
  }
})()
