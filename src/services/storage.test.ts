import StorageService from './storage'

const dummyData = ['test1', 'test2']

describe('storage service', () => {
  describe('#store', () => {
    it('should store an object as json string', async () => {
      const redis = {
        store: jest.fn().mockImplementation(),
        fetch: jest.fn().mockImplementation()
      }

      const taskService = StorageService(redis)

      await taskService.store(dummyData)

      expect(redis.store.mock.calls[0][0]).toEqual(JSON.stringify(dummyData))
    })
  })

  describe('#fetch', () => {
    it('should fetch the tasks from redis', async () => {
      const redis = {
        store: jest.fn().mockImplementation(),
        fetch: jest.fn().mockImplementation()
      }

      const taskService = StorageService(redis)

      redis.fetch.mockResolvedValue(JSON.stringify(dummyData))

      expect(await taskService.fetch()).toEqual(dummyData)
    })

    it('should return empty array if there was nothing on redis', async () => {
      const redis = {
        store: jest.fn().mockImplementation(),
        fetch: jest.fn().mockImplementation()
      }

      const taskService = StorageService(redis)

      redis.fetch.mockResolvedValue(undefined)

      expect(await taskService.fetch()).toEqual([])
    })
  })
})
