import { Redis } from '../libs/redis'

export interface StorageService {
  store<T>(data: T[]): void
  fetch<T>(): Promise<T[]>
}

export default (redis: Redis): StorageService => {
  function store<T>(data: T[]): void {
    redis.store(JSON.stringify(data))
  }

  function fetch<T>(): Promise<T[]> {
    return redis.fetch().then((data: string) => {
      if (!data) {
        return []
      }

      return JSON.parse(data)
    })
  }

  return {
    store,
    fetch
  }
}
