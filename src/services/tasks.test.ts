import * as Uuid from 'uuid'

import TasksService from './tasks'

import { dummyTask } from '../tests/fixtures'

describe('tasks service', () => {
  describe('#addNewTasks', () => {
    it('should store the tasks into storage', async () => {
      const storageService = {
        store: jest.fn().mockImplementation(),
        fetch: () => Promise.resolve([])
      }

      const taskService = TasksService(storageService)

      await taskService.addNewTasks(['test1'])

      expect(storageService.store.mock.calls).toHaveLength(1)
    })

    it('should append the new tasks to the old ones', async () => {
      const storageService = {
        store: jest.fn().mockImplementation(),
        fetch: (): any => Promise.resolve([dummyTask('<id-1>', 'test1'), dummyTask('<id-2>', 'test2')])
      }

      const taskService = TasksService(storageService)

      jest.spyOn(Uuid, 'v4').mockReturnValue('<id-3>')

      await taskService.addNewTasks(['test3'])

      expect(storageService.store.mock.calls[0][0]).toEqual(
        expect.objectContaining([
          dummyTask('<id-1>', 'test1'),
          dummyTask('<id-2>', 'test2'),
          dummyTask('<id-3>', 'test3')
        ])
      )
    })
  })

  describe('#deleteTask', () => {
    it('should mark the task as completed', async () => {
      const storageService = {
        store: jest.fn().mockImplementation(),
        fetch: (): any => Promise.resolve([dummyTask('<id-1>', 'test1'), dummyTask('<id-2>', 'test2')])
      }

      const taskService = TasksService(storageService)

      await taskService.deleteTask(['<id-1>'])

      expect(storageService.store.mock.calls[0][0]).toEqual(
        expect.objectContaining([dummyTask('<id-1>', 'test1', true), dummyTask('<id-2>', 'test2')])
      )
    })
  })

  describe('#getList', () => {
    it('should return the list of tasks', async () => {
      const storageService = {
        store: jest.fn().mockImplementation(),
        fetch: (): any => Promise.resolve([dummyTask('<id-1>', 'test1'), dummyTask('<id-2>', 'test2')])
      }

      const taskService = TasksService(storageService)

      expect(await taskService.getList()).toEqual(
        expect.objectContaining([dummyTask('<id-1>', 'test1'), dummyTask('<id-2>', 'test2')])
      )
    })

    it('should return only the incomplete tasks', async () => {
      const storageService = {
        store: jest.fn().mockImplementation(),
        fetch: (): any => Promise.resolve([dummyTask('<id-1>', 'test1', true), dummyTask('<id-2>', 'test2')])
      }

      const taskService = TasksService(storageService)

      expect(await taskService.getList()).toEqual(expect.objectContaining([dummyTask('<id-2>', 'test2')]))
    })
  })
})
