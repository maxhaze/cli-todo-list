import { transformTasks } from '../transformers/tasks'
import { StorageService } from './storage'

export interface TasksService {
  addNewTasks(data: string[]): Promise<void>
  deleteTask(ids: string[]): void
  getList(): Promise<Task[]>
}

export default (storageService: StorageService): TasksService => {
  async function addNewTasks(rawTasks: string[]): Promise<void> {
    const tasks = await storageService.fetch<Task>()

    const transformedRawTasks = transformTasks(rawTasks)

    await storageService.store<Task>([...tasks, ...transformedRawTasks])
  }

  async function deleteTask(ids: string[]): Promise<void> {
    const tasks = await storageService.fetch<Task>()

    ids.map((id: string) => {
      const foundTask = tasks.find((task: Task) => task.id === id)

      foundTask.isCompleted = true
    })

    await storageService.store<Task>(tasks)
  }

  function getList(): Promise<Task[]> {
    return storageService.fetch<Task>().then((tasks: Task[]) => tasks.filter((task: Task) => !task.isCompleted))
  }

  return {
    addNewTasks,
    deleteTask,
    getList
  }
}
