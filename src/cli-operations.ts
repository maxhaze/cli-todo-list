import * as Prompts from 'prompts'

import { TasksService } from './services/tasks'
import { transformToChoices } from './transformers/tasks'

export interface CliOperations {
  handleAdd(): Promise<void>
  handleList(tasks: Task[]): Promise<void>
}

export default (tasksService: TasksService): CliOperations => {
  async function handleAdd(): Promise<void> {
    const newTasks = []

    while (true) {
      const { newTask } = await Prompts({
        type: 'text',
        name: 'newTask',
        message: 'What is your new Task? (enter empty to stop)'
      })

      if (!newTask.trim()) {
        break
      }

      newTasks.push(newTask.trim())
    }

    await tasksService.addNewTasks(newTasks)
  }

  async function handleList(tasks: Task[]): Promise<void> {
    const choices = transformToChoices(tasks)

    if (choices.length === 0) {
      return
    }

    const { deletedTasks } = await Prompts({
      type: 'multiselect',
      name: 'deletedTasks',
      message: 'Your Tasks (Select to mark as complete)',
      choices
    })

    return deletedTasks && tasksService.deleteTask(deletedTasks)
  }

  return {
    handleAdd,
    handleList
  }
}
