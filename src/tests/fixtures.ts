export function dummyTask(id: string, title: string, isCompleted: boolean = false): Task {
  return {
    id,
    title,
    isCompleted
  }
}
