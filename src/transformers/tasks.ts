import { v4 } from 'uuid'

export function transformTasks(rawTasks: string[]): Task[] {
  return rawTasks.map(
    (title: string): Task => ({
      title,
      isCompleted: false,
      id: v4()
    })
  )
}

export function transformToChoices(tasks: Task[]): TaskChoice[] {
  return tasks.map(
    ({ id, title }: Task): TaskChoice => ({
      title,
      value: id
    })
  )
}
