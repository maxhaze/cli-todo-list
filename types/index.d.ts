type Func = (...arg) => any

interface Task {
  id: string
  title: string
  isCompleted: boolean
}

interface TaskChoice {
  title: string
  value: string
}
